$( function() {
      from = $( "#from" )
        .datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          dateFormat:"yy-mm-dd",

          onSelect: function (selectedDate) {
            endDate = $(this).datepicker('getDate');
            var final=new Date(endDate);
            var min_date = new Date(endDate);
            $('#to').datepicker('option', 'minDate',min_date);
            final.setDate(endDate.getDate() + 7);
            
            $('#to').datepicker('option', 'maxDate',final);
        }
        })
        ,
      to = $( "#to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        dateFormat:"yy-mm-dd",

        onSelect: function (selectedDate) {
            endDate = $(this).datepicker('getDate');
            var final=new Date(endDate);
            var min_date = new Date(endDate);
            $('#from').datepicker('option', 'maxDate',min_date);
            final.setDate(endDate.getDate() - 7);
            
            $('#from').datepicker('option', 'minDate',final);
        }
      })
      
 
   
  } );

function search() {
    var btn = $("#search_btn");
    

    var base_url = $("#base_url").val();

    var fromDate = $("#from").val();
    var toDate = $("#to").val();
    var dataString = "from_date="+fromDate+"&to_date="+toDate;

    if(fromDate == '') {
        $("#from_error").html("Required");
        setTimeout(function() {
            $("#from_error").html("");
        }, 3000);
        return false;
    } else if(toDate == '') {
        $("#to_error").html("Required");
        setTimeout(function() {
            $("#to_error").html("");
        }, 3000);
        return false;
    } else {
        btn.button('loading');
        $.ajax({
            type : 'POST',
            data : dataString,
            url : base_url + 'Asteroid/search',
    
            success : function(response) {
                var asteroid_data = JSON.parse(response);
    
                btn.button('reset');
    
                var html = "<div class='row'>";
                html += "<div class='col-md-4'><h4>Fastest Asteroid</h4>";
                html += "<div><label>Asteroid ID:</label> "+asteroid_data.fastest_asteroid_id+"</div>";
                html += "<div><label>Speed (km/h):</label> "+asteroid_data.fastest_asteroid_speed+"</div>";
                html += "</div>";
    
                html += "<div class='col-md-4'><h4>Closest Asteroid</h4>";
                html += "<div><label>Asteroid ID:</label> "+asteroid_data.closest_asteroid_id+"</div>";
                html += "<div><label>Distance (km):</label> "+asteroid_data.closest_asteroid_distance+"</div>";
                html += "</div>";
    
                html += "<div class='col-md-4'><h4>Average Size of the Asteroids in kilometers</h4>";
                html += "<div><label>Minimum Average Diameter:</label> "+asteroid_data.min_average_diameter+"</div>";
                html += "<div><label>Maximum Average Diameter:</label> "+asteroid_data.max_average_diameter+"</div>";
                html += "</div>";
    
                html += "</div>";
                $("#asteroid_data").html(html);
    
    
    
    
                var ctx = document.getElementById('asteroidChart').getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: asteroid_data.date_array,
                        datasets: [{
                            label: 'Total number of asteroids',
                            data: asteroid_data.asteroids_count_array,
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
            },
            error : function(jqXHR, exception) {
                btn.button('reset');
            }
        })
    }

    
}