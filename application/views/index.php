<!doctype html>
<html>
    <head>
        <title>Asteroid Neo App</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/jquery/css/jquery-ui.css') ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/charts/css/Chart.min.css') ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/custom/css/asteroid.css') ?>"/>
    </head>
    <body>
        <div class="container">
        	<h3>Asteroid Neo App</h3>
        	<div class="row">
        		<div class="col-md-4">
        			<label>From Date <span class="error">*</span> <span class="error" id="from_error"></span></label> 
    	    		<input type="text" id="from" class="form-control" readonly="">
	        	</div>
	        	<div class="col-md-4">
        			<label>To Date <span class="error">*</span>  <span class="error" id="to_error"></span></label>
    	    		<input type="text" id="to" class="form-control" readonly="">
	        	</div>
	        	<div class="col-md-4">
        			<div class="row">&nbsp;</div>
        			<input type="hidden" id="base_url" value="<?php echo base_url() ?>">
    	    		<button type="button" id="search_btn" class="btn btn-success" onclick="search()" data-loading-text="Searching...">Submit</button>
	        	</div>
        	</div>
        	<div id="asteroid_data"></div>
        	<canvas id="asteroidChart" width="400" height="300"></canvas>
        </div>
    </body>
    <script src="<?php echo base_url('assets/jquery/js/jquery-1.12.4.js') ?>"></script>
    <script src="<?php echo base_url('assets/jquery/js/jquery-ui.js') ?>"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>
    <script src="<?php echo base_url('assets/charts/js/Chart.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/custom/js/asteroid.js') ?>"></script>
</html>