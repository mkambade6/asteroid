<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Asteroid extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('index.php');
    }

    public function search() {
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');

        $url = "https://api.nasa.gov/neo/rest/v1/feed?start_date=".$from_date."&end_date=".$to_date."&api_key=UiNE31YRlMEzJ1WbBho84Rupux2BK1V6nr8GyABY";

        $neo_stats = file_get_contents($url);
        
        $neo_stats_object = json_decode($neo_stats);
        $near_earth_object = $neo_stats_object->near_earth_objects;

        $fastest_asteroid_id = '';
        $fastest_asteroid_speed = 0;

        $closest_asteroid_id = '';
        $closest_asteroid_distance = INF;

        $min_diameter_total = 0;
        $max_diameter_total = 0;

        $date_array = array();
        $asteroids_count_array = array();

        foreach($near_earth_object as $near_earth_date_key => $near_earth_date_row) {
            foreach($near_earth_date_row as $near_earth_date_data){
                if($near_earth_date_data->close_approach_data[0]->relative_velocity->kilometers_per_second >= $fastest_asteroid_speed) {
                    $fastest_asteroid_speed = $near_earth_date_data->close_approach_data[0]->relative_velocity->kilometers_per_second;
                    $fastest_asteroid_id = $near_earth_date_data->id;
                }

                if($near_earth_date_data->close_approach_data[0]->miss_distance->kilometers <= $closest_asteroid_distance) {
                    $closest_asteroid_distance = $near_earth_date_data->close_approach_data[0]->miss_distance->kilometers;
                    $closest_asteroid_id = $near_earth_date_data->id;
                }

                $min_diameter_total = $min_diameter_total + $near_earth_date_data->estimated_diameter->kilometers->estimated_diameter_min ;
                $max_diameter_total = $max_diameter_total + $near_earth_date_data->estimated_diameter->kilometers->estimated_diameter_max ;

            }

            array_push($date_array, $near_earth_date_key);
            array_push($asteroids_count_array, count($near_earth_date_row));

        }
        $min_average_diameter = $min_diameter_total/$neo_stats_object->element_count;
        $max_average_diameter = $max_diameter_total/$neo_stats_object->element_count;
        
        $data = array(
            'fastest_asteroid_id' => $fastest_asteroid_id,
            'fastest_asteroid_speed' => $fastest_asteroid_speed,
            'closest_asteroid_id' => $closest_asteroid_id,
            'closest_asteroid_distance' => $closest_asteroid_distance,
            'min_average_diameter' => $min_average_diameter,
            'max_average_diameter' => $max_average_diameter,
            'date_array' => $date_array,
            'asteroids_count_array' => $asteroids_count_array,
        );

        echo json_encode($data);
        exit;
    }
}
